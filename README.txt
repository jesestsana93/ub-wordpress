Curso de Udemy: "Universidad Bootstrap: crea un sitio web real (Wordpress)" por Erick Mines
Duración: 10:50 hrs

Herramientas:
* Google Chrome como navegador para realizar los testeos
* Visual studio code como editor de codigo
* Koala como preprocesador de textos SASS
* prepros.io como alternativa a koala, y https://scout-app.io/

* Barra de neavegacion (ejemplo navbar): http://simia.com.au/rd-navbar.html
http://cms.devoffice.com/coding-dev/rd-navbar/documentation/index.php?lang=en&section=howtouse

* Efecto parrallax: https://github.com/marrio-h/universal-parallax 
* Extensión para saber medida de un elemento en Chrome: page ruler

Extensiones para visual studio code: https://marketplace.visualstudio.com/
* Live Server: detecta algún cambio en el proyecto actualizandolo
* Class autocomplete for HTML, otra alternativa es HTML Class suggestions
* SCSS Formatter
* Otras que tiene pero no menciona: Debugger for Chrome, Debugger for Firefox, 
ESLint, jshint, Beautify, Format HTML in PHP, Bookmarks

Url del sitio terminado: http://www.aulaideal.com/ubt2/